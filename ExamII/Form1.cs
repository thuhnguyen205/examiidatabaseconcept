﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ExamII
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            using (var context = new PetStoreEntities())
            {
                var qCategory =
                    from category in context.Categories
                    orderby category.Category1
                    select category;
                cboCategory.DisplayMember = "Category1";
                cboCategory.ValueMember = "Category1";
                cboCategory.DataSource = qCategory.ToList();
            }
            cboCategory_SelectedIndexChanged(null, null);
        }

        private void cboCategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboCategory.SelectedIndex >= 0)
            {
                string strSelectedCategory = cboCategory.SelectedValue.ToString();
                dgvMerchandise.Rows.Clear();
                using (var context = new PetStoreEntities())
                {
                    var qMerchandisebyCategory =
                        from merchandise in context.Merchandises
                        where merchandise.Category == strSelectedCategory
                        orderby merchandise.Description
                        select merchandise;

                    foreach (var merchandise in qMerchandisebyCategory)
                    {
                        int iNewRowIndex = dgvMerchandise.Rows.Add();
                        DataGridViewRow dgvrNewRow = dgvMerchandise.Rows[iNewRowIndex];
                        dgvrNewRow.Cells["DescriptionColumn"].Value = merchandise.Description;
                        dgvrNewRow.Cells["ListPriceColumn"].Value = merchandise.ListPrice;
                        dgvrNewRow.Cells["StockColumn"].Value = merchandise.QuantityOnHand;
                        dgvrNewRow.Cells["ItemIDColumn"].Value = merchandise.ItemID;
                    }
                }
            }
        }
        private void btnCategory_Click(object sender, EventArgs e)
        {
            if (dgvMerchandise.SelectedRows.Count > 0)
            {
                DataGridViewRow dgvrSelectedMerchandise = dgvMerchandise.SelectedRows[0];
                AddCategory frmAddCategory = new AddCategory();
                DialogResult drResult = frmAddCategory.ShowDialog();
                if (drResult == DialogResult.OK)
                {
                    using (var context = new PetStoreEntities())
                    {
                        var sNewCategory = new Category
                        {
                            Category1 = frmAddCategory.CategoryName,
                            Registration = frmAddCategory.Registration,
                        };
                        context.Categories.Add(sNewCategory);
                        context.SaveChanges();
                    }
                    Form1_Load(null, null);
                }
            }
        }
    }
}

       
            
       