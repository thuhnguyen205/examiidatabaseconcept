﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ExamII
{
    public partial class AddCategory : Form
    {
        public AddCategory()
        {
            InitializeComponent();
        }
        public string CategoryName
        {
            set
            {
                txtCategoryName.Text = value;
            }
            get
            {
                return txtCategoryName.Text;
            }
        }

        public string Registration
        {
            set
            {
                txtRegistration.Text = value;
            }
            get
            {
                return txtRegistration.Text;
            }
        }
    }
}
